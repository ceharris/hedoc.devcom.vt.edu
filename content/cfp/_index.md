---
title: Call for Proposals
layout: "single"
---

In order for this to be successful, we need your help, your content, and your energy! We recognize that many in our industry may not get the opportunity to present and talk about the things they are working on! So, consider this your chance! We’d love to have both seasoned presenters and first-timers! All are welcome to present!

### Some things to know

- The entire conference will be virtual and take place on the afternoons of June 2-4 (1-5 EDT)
- The conference will be limited to 1500 participants from the higher-ed community. Basically, if you can login via InCommon, you can attend… for free!
- We will be leveraging a platform that can stream your talk and have a live chat/Q&A
- All sessions will be a maximum of 30 minutes
- If your topic requires more than 30 minutes, feel free to split your idea into multiple sessions. Please submit each session individually.
- We strongly recommend pre-recording your session to minimize the chance of network issues during the session. In addition, it also helps us close-caption the talk before the event. All recorded sessions will need to be submitted before May 21.
- You are welcome (and encouraged) to pre-record a portion of your talk and then do a live Q&A segment at the end. A good example might be spending 20 minutes with pre-recorded material and then the final 10 minutes live.
- We will have training sessions for all speakers who are unfamiliar with recording talks. And don’t worry… they don’t have to be absolutely perfect video productions!
- While the recorded sessions are playing, speakers are encouraged to interact with participants via the live chat on the platform.
- Any and all are welcome to submit a CFP. We welcome everyone regardless of gender, sexual orientation, age, disability, physical appearance, body size, race, veteran status, religion, or immigration status.


### What makes a good CFP submission?

When writing your proposal, you should consider the following:

- **Settle on a topic** - choose a topic that you are excited about and knowledgeable on. While you certainly don’t have to be an expert, it does need to be a topic you are comfortable speaking about and answering questions about.
- **Pick a fun title** - choose a title that stands out, but make sure it is connected to your content. You want to encourage your audience to dive in, read the abstract, and attend your session!
- **Choose an audience** - talks that target everyone (managers/directors, developers, architects, etc.) are typically too broad to have any many valuable takeaways. Pick an audience and make sure you target them in your abstract and your talk content.
- **Identify takeaways** - what can your audience expect to learn and take away from your session? Are you going to provide any demos, sample code, or templates?
- **Write your abstract** - a good abstract is long enough to catch your audience and describe your session, but not a full essay.


### Topic Ideas

With this being our first year, we want to keep the content as open as possible. But, topics should be focused around the DevOps lifecycle and the ability to provide value to our organizations. Ideas for topics might include, but are certainly not limited to:

- Intros or deep-dives into tooling you use
- Anything related to containers (intro, building best practices, monitoring, deploying, etc.)
- Anything related to deploying applications on cloud infrastructure
- Agile development and project management
- Securing your applications
- Monitoring and alerting
- Non-traditional DevOps (applying DevOps practices in places you wouldn’t normally find it)

### Important Dates

- March 31 - CFP opens
- April 19 - CFP closes and review beings
- April 26 - all speakers notified, agenda published, general registration opens
- Dates TBD in Late April/May - office hours to provide presenter support, training, run-throughs, etc.
- May 21 - recorded session submission deadline
- May 31 - practice session using the platform
- June 2-4 - the conference itself


{{< cfp-button >}}
