---
title: Home
hideTime: true
slug: "/"
hideTitle: true
---


# Welcome to the Higher-Ed DevOps Conference!

Hello and welcome! On the afternoons (1-5 EDT) of June 2-4, we are excited to have a place to share training, best practices, and more! Due to the on-going pandemic, this inaugural conference will be presented in a virtual-only format. The majority of session material will be pre-recorded (to minimize network availability issues), but live chat and Q&As will still be available.

This event is a satellite event of the [Cloud Forum](https://blogs.cornell.edu/cloudforum/) founded by Cornell University.

{{< alert text="This site is still being actively worked on, so check back for updates!" >}}


## The Planning Committee

The planning committee consists of individuals from the universities listed below. They are excited to get this conference going and hope it is a welcoming and inclusive environment for all!

{{< contributing-universities >}}


## Frequently Asked Questions

### What do you mean by "DevOps"?

Great question! If you ask five people to define DevOps, you might get ten answers! For the purposes of this conference, we are defining as the following:

> DevOps encompasses all of the practices needed to quickly, safely, and reliably respond to the needs of our organizations and/or users.

This captures ideas around project management, application development, testing, deployment, cloud architecture, and more!


### Who is invited to attend?

If you are faculty or staff of an InCommon institution, you are welcome to attend _for free_!


### What content will be presented?

The goal of this conference is to help connect and share ideas and trainings about everything DevOps amongst ourselves. While we don't have the specific agenda yet (please [submit a proposal!!!](/cfp)), we hope to have content from all aspects of DevOps.


### Who will be presenting?

Great question! Hopefully... you! As this conference is providing the ability to share content amongst ourselves, we want speakers to be amongst ourselves too! We'll have seasoned speakers, as well as first-timers! If you have something to share, we want to help you do so! This can be a great opportunity to become a first-time speaker!

### Why is the conference taking place across three days in the afternoon?

We recognize that there are differences between in-person and virtual conferences. We know that most people attend virtual conferences while still having to perform their normal job functions. To help this, we decided it would be best to still you half of your day to respond to email, respond to operational issues, or whatever else you have to do!
